defmodule MicroChat.Repo.Migrations.CreateTickets do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :email, :string, null: false
      add :name, :string
      add :password_hash, :string
      add :last_login, :utc_datetime
      add :is_admin_account_created, :boolean, null: false, default: false
      timestamps(type: :utc_datetime)
    end

    create unique_index(:users, [:email])

    create table(:organizations) do
      add :name, :string, null: false
      timestamps(type: :utc_datetime)
    end

    create table(:roles) do
      add :name, :string, null: false
    end

    execute("""
      INSERT INTO roles (id, name) VALUES
        (1, 'owner'),
        (2, 'admin'),
        (3, 'agent'),
        (4, 'contact')
    ;
    """)

    create table(:users_organizations) do
      add :user_id, references(:users), null: false
      add :organization_id, references(:organizations), null: false
      add :role_id, references(:roles), null: false

      timestamps(type: :utc_datetime)
    end

    create unique_index(:users_organizations, [:user_id, :organization_id])

    create table(:sessions) do
      add :user_organization_id, references(:users_organizations), null: false
      timestamps(type: :utc_datetime)
    end

    create table(:tickets) do
      add :number, :integer, null: false
      add :subject, :string, null: false
      add :status, :integer, null: false
      add :organization_id, references(:organizations), null: false
      add :assigned_agent_id, references(:users_organizations), null: true
      add :contact_id, references(:users_organizations), null: false

      timestamps(type: :utc_datetime)
    end

    create unique_index(:tickets, [:organization_id, :number])

    create table(:ticket_organization_seq_tracker) do
      add :organization_id, references(:organizations), null: false
      add :last_id, :bigint, null: false
    end

    create unique_index(:ticket_organization_seq_tracker, [:organization_id])

    execute("""
      CREATE OR REPLACE FUNCTION get_next_ticket_number(org_id bigint) RETURNS bigint AS $$
        UPDATE ticket_organization_seq_tracker
        SET last_id = last_id + 1
        WHERE organization_id = org_id
        RETURNING last_id
      $$ LANGUAGE SQL;
    """)

    execute("""
      COMMENT ON FUNCTION get_next_ticket_number(bigint) IS 'Returns the next ticket number by organization';
    """)

    execute("""
      CREATE OR REPLACE FUNCTION set_ticket_number() returns trigger as $$
        begin
          if NEW."number" is null then
                NEW."number" := get_next_ticket_number(NEW.organization_id);
            end if;
            return new;
        end;
      $$ language plpgsql;
    """)

    execute("""
      COMMENT ON FUNCTION set_ticket_number() IS 'Sets the number column from tickets using get_next_ticket_number when number is null';
    """)

    execute("""
      create trigger trig_set_ticket_number
      before insert
      on tickets
      for each row
      execute procedure set_ticket_number();
    """)

    create table(:messages) do
      add :content, :text
      add :ticket_id, references(:tickets), null: false
      add :user_organization_id, references(:users_organizations), null: false

      timestamps(type: :utc_datetime)
    end
  end
end
