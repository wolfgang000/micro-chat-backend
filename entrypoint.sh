#!/bin/sh
# Docker entrypoint script.

# Ref: https://docs.akkeris.io/support/url-parsing.html#shell-bshzshsh

proto="$(echo $DATABASE_URL | grep :// | sed -e's,^\(.*://\).*,\1,g')"
# remove the protocol
url="$(echo ${DATABASE_URL/$proto/})"
# extract the user_pass
db_userpass="$(echo $url | grep @ | cut -d@ -f1)"
# extract the pass 
db_pass="$(echo $db_userpass | grep : | cut -d: -f2)"
# extract the user
if [ -n "$db_pass" ]; then
  db_user="$(echo $db_userpass | grep : | cut -d: -f1)"
else
  db_user=$db_userpass
fi
# extract the host and port
hostport="$(echo ${url/$db_userpass@/} | cut -d/ -f1)"
# by request host without port
db_host="$(echo $hostport | sed -e 's,:.*,,g')"
# by request - try to extract the port
db_port="$(echo $hostport | sed -e 's,^.*:,:,g' -e 's,.*:\([0-9]*\).*,\1,g' -e 's,[^0-9],,g')"
# extract the path (if any)
path="$(echo $url | grep / | cut -d/ -f2-)"

# Wait until Postgres is ready
while ! pg_isready -q -h $db_host -p $db_port -U $db_user
do
  echo "$(date) - waiting for database to start"
  sleep 2
done

./prod/rel/micro_chat/bin/micro_chat eval MicroChat.Release.migrate

./prod/rel/micro_chat/bin/micro_chat start