# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :micro_chat,
  ecto_repos: [MicroChat.Repo]

# Configures the endpoint
config :micro_chat, MicroChatWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "2sVVBliOu439ixKFs9Ex0HDrckCnr5tw20C0QJ8u8cYceP0K5+4mMFcXsICIpEqi",
  render_errors: [view: MicroChatWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: MicroChat.PubSub,
  live_view: [signing_salt: "qRCCfDxM"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
