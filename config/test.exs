use Mix.Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :micro_chat, MicroChat.Repo,
  username: "test_user",
  password: "test_password",
  database: "micro_chat_test#{System.get_env("MIX_TEST_PARTITION")}",
  hostname: System.get_env("DB_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :micro_chat, MicroChatWeb.Endpoint,
  http: [port: 4002],
  server: false,
  admin_url: "http://localhost:4000",
  endpoint_scheme: "http",
  client_root_host: "example.com"

# Print only warnings and errors during test
config :logger, level: (System.get_env("LOGGER_LEVEL") || "warn") |> String.to_atom()

config :micro_chat, MicroChat.Mailer,
  adapter: Bamboo.TestAdapter,
  default_from_address: "test@mail.com"

config :argon2_elixir, t_cost: 1, m_cost: 8
