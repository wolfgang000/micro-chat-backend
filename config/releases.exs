# In this file, we load production configuration and secrets
# from environment variables. You can also hardcode secrets,
# although such is generally not recommended and you have to
# remember to add this file to your .gitignore.
import Config

defmodule ReleaseUtil do
  def get_env_or_raise_error(var_name),
    do:
      System.get_env(var_name) ||
        raise("""
        environment variable #{var_name} is missing.
        """)
end

secret_key_base = ReleaseUtil.get_env_or_raise_error("SECRET_KEY_BASE")
mailgun_apy_key = ReleaseUtil.get_env_or_raise_error("MAILGUN_API_KEY")
mailgun_domain = ReleaseUtil.get_env_or_raise_error("MAILGUN_DOMAIN")
default_from_address = ReleaseUtil.get_env_or_raise_error("DEFAULT_FROM_ADDRESS")
db_url = ReleaseUtil.get_env_or_raise_error("DATABASE_URL")
db_pool_size = System.get_env("POOL_SIZE") || "10"

allow_origin = ReleaseUtil.get_env_or_raise_error("ALLOW_ORIGIN")
endpoint_scheme = ReleaseUtil.get_env_or_raise_error("ENDPOINT_SCHEME")

client_root_host = ReleaseUtil.get_env_or_raise_error("CLIENT_ROOT_HOST")

logger_level = (System.get_env("LOGGER_LEVEL") || "info") |> String.to_atom()

config :logger, level: logger_level

config :micro_chat, MicroChat.Repo,
  # ssl: true,
  url: db_url,
  pool_size: String.to_integer(db_pool_size)

config :micro_chat, MicroChatWeb.Endpoint,
  server: true,
  http: [
    port: String.to_integer(System.get_env("PORT") || "4000"),
    transport_options: [socket_opts: [:inet6]]
  ],
  secret_key_base: secret_key_base,
  check_origin: [allow_origin],
  endpoint_scheme: endpoint_scheme,
  client_root_host: client_root_host

config :micro_chat, MicroChat.Mailer,
  adapter: Bamboo.MailgunAdapter,
  domain: mailgun_domain,
  api_key: mailgun_apy_key,
  hackney_opts: [
    recv_timeout: :timer.minutes(1)
  ],
  default_from_address: default_from_address
