# MicroChat

To start your Phoenix server:

- Install dependencies with `mix deps.get`
- Create and migrate your database with `mix ecto.setup`
- Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Run locally with env vars

```
source .env.local.dev && mix phx.server
```

## Build production image localy

```
docker build -t myimage -f Dockerfile.prod .
```

## Test production build

```
docker run --net=host --env-file ./.env.example.prod myimage
```

## Dokku Deployment

```
dokku apps:create micro-chat-back
dokku plugin:install https://github.com/dokku/dokku-postgres.git
dokku postgres:create micro-chat-db
dokku postgres:link micro-chat-db micro-chat-back
dokku docker-options:add micro-chat-back build '--file Dockerfile.prod'
dokku config:set micro-chat-back \
  # Set the variables from .env.example
```
