defmodule MicroChatWeb.UserController do
  use MicroChatWeb, :controller
  action_fallback MicroChatWeb.FallbackController

  alias MicroChat.Accounts

  def index(%{assigns: %{current_organization_id: organization_id}} = conn, params) do
    users = Accounts.list_users_organizations(organization_id, params)
    render(conn, "index.json", users: users)
  end
end
