defmodule MicroChatWeb.TicketController do
  use MicroChatWeb, :controller
  action_fallback MicroChatWeb.FallbackController

  alias MicroChat.Chat
  alias MicroChatWeb.TicketHelper

  def index(%{assigns: %{current_organization_id: organization_id}} = conn, _params) do
    tickets = Chat.list_organizations_tickets(organization_id)
    render(conn, "index.json", tickets: tickets)
  end

  # TODO: add validations
  # * user_organization must belong to the same organization
  # * user_organization must have "admin", "owner" or "agent" role
  def create(
        %{
          assigns: %{
            current_organization_id: organization_id,
            current_user_organization: %{id: user_organization_id}
          }
        } = conn,
        %{
          "ticket" =>
            %{
              "subject" => _,
              "contact" => %{"email" => _}
            } = ticket_params,
          "message" => message_params
        }
      ) do
    with {:ok, %{create_ticket: ticket, create_message: message}} <-
           Chat.create_ticket_with_message(
             ticket_params
             |> Map.put("organization_id", organization_id),
             message_params
             |> Map.put("user_organization_id", user_organization_id)
           ),
         {:ok, ticket_with_number} <- Chat.get_ticket_by_id(ticket.id) do
      TicketHelper.maybe_send_assigned_agent_email(ticket_with_number)
      TicketHelper.maybe_send_ticket_creation_client_email(ticket_with_number, message.content)

      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.ticket_path(conn, :show, ticket_with_number))
      |> render("show_created.json", ticket: ticket_with_number)
    end
  end

  def create(conn, _params), do: send_resp(conn, 400, "")

  def show(%{assigns: %{current_organization_id: organization_id}} = conn, %{
        "ticket_number" => ticket_number
      }) do
    with {:ok, ticket} <- Chat.get_ticket(ticket_number, organization_id) do
      render(conn, "show.json", ticket: ticket)
    end
  end

  def get_messages(%{assigns: %{current_organization_id: organization_id}} = conn, %{
        "ticket_number" => ticket_number
      }) do
    messages = Chat.get_messages_by_ticket(ticket_number, organization_id)
    render(conn, "index.json", messages: messages)
  end

  def update(%{assigns: %{current_organization_id: organization_id}} = conn, %{
        "ticket_number" => ticket_number,
        "ticket" => ticket_params
      }) do
    with {:ok, ticket} <- Chat.get_ticket(ticket_number, organization_id),
         {:ok, updated_ticket} <- Chat.update_ticket(ticket, ticket_params) do
      if ticket.assigned_agent_id != updated_ticket.assigned_agent_id do
        TicketHelper.maybe_send_assigned_agent_email(updated_ticket)
      end

      render(conn, "show.json", ticket: updated_ticket)
    end
  end

  # def delete(conn, %{"id" => id}) do
  #   ticket = Core.get_ticket!(id)

  #   with {:ok, %Ticket{}} <- Core.delete_ticket(ticket) do
  #     send_resp(conn, :no_content, "")
  #   end
  # end
end
