defmodule MicroChatWeb.AccountController do
  use MicroChatWeb, :controller

  alias MicroChat.Accounts
  alias MicroChatWeb.TokenHelper

  action_fallback MicroChatWeb.FallbackController

  # TODO: replace session.id with a random session_id
  def login(conn, %{"user" => %{"email" => email, "password" => password}}) do
    with {:ok, user} <- Accounts.authenticate_user(String.downcase(email), password),
         {:ok, user} <- Accounts.update_user_last_login_date(user),
         {:ok, user_organization} <- Accounts.get_first_user_organization(user.id),
         {:ok, session} <- Accounts.create_session(%{user_organization_id: user_organization.id}) do
      conn
      |> put_session(:current_session_id, session.id)
      |> put_status(:ok)
      |> render("success_login.json",
        user: Map.put(user, :user_organization_id, user_organization.id)
      )
    end
  end

  def login_client(%{assigns: %{current_tenant_organization: organization}} = conn, %{
        "user" => %{"email" => email, "code" => _code}
      }) do
    with {:ok, user} <- Accounts.get_user_by_email(String.downcase(email)),
         {:ok, user} <- Accounts.update_user_last_login_date(user),
         {:ok, user_organization} <- Accounts.get_user_organization(user.id, organization.id),
         {:ok, session} <- Accounts.create_session(%{user_organization_id: user_organization.id}) do
      conn
      |> put_session(:current_session_id, session.id)
      |> put_status(:ok)
      |> render("success_login.json",
        user: Map.put(user, :user_organization_id, user_organization.id)
      )
    end
  end

  def login_client(
        %{assigns: %{current_tenant_organization: %{id: tenant_organization_id}}} = conn,
        %{
          "user" => %{"token" => token}
        }
      ) do
    with {:ok, %{user_id: user_id, organization_id: ^tenant_organization_id}} <-
           TokenHelper.verify_tenant_token(conn, token, max_age: 1_209_600),
         {:ok, user} <- Accounts.get_user_by_id(user_id),
         {:ok, user} <- Accounts.update_user_last_login_date(user),
         {:ok, user_organization} <-
           Accounts.get_user_organization(user.id, tenant_organization_id),
         {:ok, session} <- Accounts.create_session(%{user_organization_id: user_organization.id}) do
      conn
      |> put_session(:current_session_id, session.id)
      |> put_status(:ok)
      |> render("success_login.json",
        user: Map.put(user, :user_organization_id, user_organization.id)
      )
    else
      {:error, :invalid} ->
        {:error, :not_found}

      {:error, :invalid} ->
        {:error, :not_found}

      {:error, :missing} ->
        {:error, :not_found}

      e ->
        e
    end
  end

  def signup(
        conn,
        %{
          "user" => user_params,
          "organization" =>
            %{
              "name" => _
            } = organization_params
        }
      ) do
    with {:ok, user_with_organization} <-
           Accounts.create_user_with_organization(
             user_params |> Map.put("is_admin_account_created", true),
             organization_params
           ) do
      conn
      |> put_status(:created)
      |> render("show_user_with_organization.json", user_with_organization)
    end
  end

  # TODO: refactor this and replace organization_id with a signed token with organization_id + role_id + email

  def signup(
        conn,
        %{"user" => user_params, "organization" => %{"id" => _} = organization_params}
      ) do
    with {:ok, user_with_organization} <-
           Accounts.create_user_and_assoc_user_to_organization(
             user_params |> Map.put("is_admin_account_created", true),
             organization_params
           ) do
      conn
      |> put_status(:created)
      |> render("show_user_with_organization.json", user_with_organization)
    end
  end

  def logout(%{assigns: %{current_session_id: session_id}} = conn, _params) do
    with {:ok, _} <- Accounts.delete_session_by_id(session_id) do
      conn
      |> clear_session()
      |> send_resp(204, "")
    end
  end

  def get_socket_token(%{assigns: %{current_session_id: session_id}} = conn, _) do
    token = TokenHelper.sign_websocket_data(conn, session_id)
    render(conn, "show_user_socket_token.json", token: token)
  end
end
