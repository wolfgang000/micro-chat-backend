defmodule MicroChatWeb.TicketView do
  use MicroChatWeb, :view
  alias MicroChatWeb.TicketView
  alias MicroChatWeb.MessageView

  def render("index.json", %{tickets: tickets}) do
    %{data: render_many(tickets, TicketView, "ticket.json")}
  end

  def render("show_created.json", %{ticket: ticket}) do
    %{
      data: %{
        id: ticket.id,
        number: ticket.number,
        subject: ticket.subject,
        inserted_at: ticket.inserted_at
      }
    }
  end

  def render("show.json", %{ticket: ticket}) do
    %{data: render_one(ticket, TicketView, "ticket.json")}
  end

  def render("ticket.json", %{ticket: ticket}) do
    %{
      id: ticket.id,
      number: ticket.number,
      subject: ticket.subject,
      status: ticket.status,
      contact: render_one(ticket.contact, TicketView, "contact.json"),
      assigned_agent_id: ticket.assigned_agent_id,
      inserted_at: ticket.inserted_at
    }
  end

  def render("contact.json", %{ticket: contact}) do
    %{
      id: contact.id,
      email: contact.user.email
    }
  end

  def render("index.json", %{messages: messages}) do
    %{data: render_many(messages, MessageView, "message.json")}
  end

  def render("show.json", %{message: message}) do
    %{data: render_one(message, MessageView, "message.json")}
  end
end
