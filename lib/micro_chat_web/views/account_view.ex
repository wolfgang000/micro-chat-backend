defmodule MicroChatWeb.AccountView do
  use MicroChatWeb, :view
  alias MicroChatWeb.AccountView

  def render("success_login.json", %{user: login_user}) do
    %{data: render_one(login_user, AccountView, "login_user.json")}
  end

  def render("login_user.json", %{account: login_user}) do
    %{
      id: login_user.id,
      name: login_user.name,
      email: login_user.email,
      user_organization_id: login_user.user_organization_id
    }
  end

  def render("show_user_with_organization.json", %{user: user, organization: organization}) do
    %{
      user: render_one(user, AccountView, "user.json"),
      organization: render_one(organization, AccountView, "organization.json")
    }
  end

  def render("user.json", %{account: user}) do
    %{
      id: user.id,
      email: user.email
    }
  end

  def render("organization.json", %{account: organization}) do
    %{
      id: organization.id,
      name: organization.name
    }
  end

  def render("show_user_socket_token.json", %{token: token}) do
    %{data: render_one(token, AccountView, "user_socket_token.json")}
  end

  def render("user_socket_token.json", %{account: token}) do
    %{token: token}
  end
end
