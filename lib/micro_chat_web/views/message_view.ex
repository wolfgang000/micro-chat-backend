defmodule MicroChatWeb.MessageView do
  use MicroChatWeb, :view
  alias MicroChatWeb.MessageView

  def render("index.json", %{messages: messages}) do
    %{data: render_many(messages, MessageView, "message.json")}
  end

  def render("show.json", %{message: message}) do
    %{data: render_one(message, MessageView, "message.json")}
  end

  def render("message.json", %{message: message}) do
    %{
      id: message.id,
      user_organization_id: message.user_organization_id,
      ticket_id: message.ticket_id,
      content: message.content,
      inserted_at: message.inserted_at
    }
  end
end
