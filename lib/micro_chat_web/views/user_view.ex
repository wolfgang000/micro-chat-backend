defmodule MicroChatWeb.UserView do
  use MicroChatWeb, :view
  alias MicroChatWeb.UserView

  def render("index.json", %{users: users}) do
    %{data: render_many(users, UserView, "user_organization.json")}
  end

  def render("show.json", %{user: user}) do
    %{data: render_one(user, UserView, "user_organization.json")}
  end

  def render("user_organization.json", %{user: user_organization}) do
    %{
      id: user_organization.id,
      inserted_at: user_organization.inserted_at,
      role_id: user_organization.role_id,
      user: render_one(user_organization.user, UserView, "user.json")
    }
  end

  def render("user.json", %{user: user}) do
    %{
      id: user.id,
      email: user.email
    }
  end
end
