defmodule MicroChatWeb.UserSocket do
  use Phoenix.Socket
  alias MicroChat.Accounts
  alias MicroChatWeb.TokenHelper

  ## Channels
  channel "ticket:*", MicroChatWeb.TicketChannel

  @impl true
  def connect(%{"token" => token}, socket, _connect_info) do
    with {:ok, session_id} <- TokenHelper.verify_websocket_token(socket, token),
         {:ok,
          %{
            user_organization:
              %{
                user_id: user_id,
                organization_id: organization_id
              } = user_organization
          }} <- Accounts.get_session_by_id_with_preloaded_data(session_id) do
      socket =
        socket
        |> assign(:current_user_id, user_id)
        |> assign(:current_organization_id, organization_id)
        |> assign(:current_user_organization, user_organization)
        |> assign(:current_session_id, session_id)

      {:ok, socket}
    else
      {:error, _reason} ->
        :error

      _ ->
        :error
    end
  end

  def connect(_params, _socket, _connect_info), do: :error

  @impl true
  def id(socket), do: "users_socket:#{socket.assigns.current_user_id}"
end
