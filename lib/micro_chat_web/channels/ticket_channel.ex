defmodule MicroChatWeb.TicketChannel do
  use MicroChatWeb, :channel
  alias MicroChatWeb.TicketHelper
  alias MicroChat.{Chat, Presence}
  alias MicroChat.Accounts.Role
  require Logger

  @impl true
  def join("ticket:" <> ticket_id, _payload, socket) do
    with {:ok, ticket} <- Chat.get_ticket_by_id(ticket_id) do
      socket = assign(socket, :current_ticket, ticket)

      send(self(), :after_join)

      {:ok, socket}
    else
      _e -> {:error, :reply}
    end
  end

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  @impl true
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  @impl true
  def handle_in(
        "message:create",
        %{"content" => content} = payload,
        %{
          assigns: %{
            current_user_organization: %{id: user_organization_id} = user_organization,
            current_ticket: ticket = %{id: ticket_id, assigned_agent_id: assigned_agent_id}
          }
        } = socket
      ) do
    with {:ok, message} <-
           Chat.create_message(%{
             content: content,
             user_organization_id: user_organization_id,
             ticket_id: ticket_id
           }) do
      serialized_message = MicroChatWeb.MessageView.render("message.json", %{message: message})
      broadcast(socket, "message:created", serialized_message)

      if(
        user_organization.id == assigned_agent_id or
          user_organization.role_id in Role.staff_role_ids()
      ) do
        TicketHelper.maybe_send_ticket_replay_to_client_email(ticket, content)
      end

      {:reply, {:ok, serialized_message}, socket}
    else
      e ->
        Logger.error("Unable to insert message, payload:#{inspect(payload)}, error:#{inspect(e)}")
        {:reply, :error, socket}
    end
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (ticket:lobby).
  @impl true
  def handle_in("shout", payload, socket) do
    broadcast(socket, "shout", payload)
    {:noreply, socket}
  end

  @impl true
  def handle_in(_, _payload, socket) do
    {:reply, :error, socket}
  end

  @impl true
  def handle_info(:after_join, socket) do
    {:ok, _} =
      Presence.track(socket, socket.assigns.current_user_id, %{
        online_at: inspect(System.system_time(:second))
      })

    push(socket, "presence_state", Presence.list(socket))
    {:noreply, socket}
  end

  # Add authorization logic here as required.
  # defp authorized?(_payload) do
  #   true
  # end
end
