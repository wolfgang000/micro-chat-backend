defmodule MicroChatWeb.Router do
  use MicroChatWeb, :router

  # TODO: add CSRF protection
  pipeline :api do
    plug(:accepts, ["json"])
    plug(:fetch_session)
    plug(:fetch_flash)
  end

  scope "/api", MicroChatWeb do
    pipe_through :api
  end

  pipeline :cookie_auth do
    plug MicroChatWeb.CookieAuthPlug
  end

  pipeline :admin_roles_auth do
    plug MicroChatWeb.AdminRolesAuthPlug
  end

  pipeline :organization_subdomain do
    plug MicroChatWeb.OrganizationSubdomainPlug
  end

  scope "/api/accounts", MicroChatWeb do
    pipe_through [:api]
    post "/login", AccountController, :login
    post "/signup", AccountController, :signup
  end

  scope "/api", MicroChatWeb do
    pipe_through [:api, :cookie_auth]
    post "/accounts/logout", AccountController, :logout
    get "/accounts/get_user_token", AccountController, :get_socket_token
  end

  # admin endpoints
  scope "/api", MicroChatWeb do
    pipe_through [:api, :cookie_auth, :admin_roles_auth]
    post "/tickets", TicketController, :create
    get "/tickets", TicketController, :index
    get "/tickets/:ticket_number", TicketController, :show
    put "/tickets/:ticket_number", TicketController, :update
    get "/tickets/:ticket_number/messages", TicketController, :get_messages

    get "/users_organizations", UserController, :index
  end

  ######################
  # Tenant's endpoints #
  ######################

  scope "/api/tenant", MicroChatWeb do
    pipe_through [:api, :organization_subdomain]

    post "/accounts/login", AccountController, :login_client
  end

  # TODO: add separate functions to TicketController exclusive for tenant's endpoints
  scope "/api/tenant", MicroChatWeb do
    pipe_through [:api, :cookie_auth]
    post "/accounts/logout", AccountController, :logout
    get "/accounts/get_user_token", AccountController, :get_socket_token

    get "/tickets/:ticket_number", TicketController, :show
    get "/tickets/:ticket_number/messages", TicketController, :get_messages
  end

  ######################

  forward(
    "/api/health",
    PlugCheckup,
    PlugCheckup.Options.new(
      json_encoder: Jason,
      checks: [
        %PlugCheckup.Check{name: "DB", module: MicroChatWeb.Healthchecks, function: :check_db}
      ]
    )
  )

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through [:fetch_session, :protect_from_forgery]
      live_dashboard "/dashboard", metrics: MicroChatWeb.Telemetry
    end

    forward "/sent_emails", Bamboo.SentEmailViewerPlug
  end
end
