defmodule MicroChatWeb.OrganizationHelper do
  defp get_endpoint_scheme,
    do: Application.get_env(:micro_chat, MicroChatWeb.Endpoint)[:endpoint_scheme]

  defp get_client_root_host,
    do: Application.get_env(:micro_chat, MicroChatWeb.Endpoint)[:client_root_host]

  def get_organization_client_url(organization) do
    "#{get_endpoint_scheme()}://#{organization.id}.#{get_client_root_host()}"
  end
end
