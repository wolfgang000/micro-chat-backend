defmodule MicroChatWeb.TicketHelper do
  alias MicroChat.{Email, Mailer, Accounts}
  alias MicroChatWeb.{OrganizationHelper, TokenHelper}

  def maybe_send_assigned_agent_email(ticket) do
    with %{assigned_agent_id: assigned_agent_id} when not is_nil(assigned_agent_id) <- ticket,
         {:ok, %{email: email}} <- Accounts.get_user_by_user_organization_id(assigned_agent_id) do
      subject = "Ticket Assigned - ##{ticket.number} #{ticket.subject}"
      from = Application.get_env(:micro_chat, MicroChat.Mailer)[:default_from_address]

      Email.ticket_assigned_email(from, email, subject, %{
        ticket_url: gen_ticket_admin_url(ticket.number)
      })
      |> Mailer.deliver_later()

      {:ok, :sent}
    else
      _ ->
        {:ok, :not_sent}
    end
  end

  def maybe_send_ticket_creation_client_email(ticket, ticket_message) do
    with %{contact_id: contact_id, organization_id: organization_id} when not is_nil(contact_id) <-
           ticket,
         {:ok, %{id: user_id, email: email}} <-
           Accounts.get_user_by_user_organization_id(contact_id),
         {:ok, organization} <- Accounts.get_organization_by_id(organization_id) do
      subject = "Ticket ##{ticket.id} - #{ticket.subject}"
      from = Application.get_env(:micro_chat, MicroChat.Mailer)[:default_from_address]

      Email.ticket_update_to_client(from, email, subject, %{
        ticket_message: ticket_message,
        ticket_number: ticket.number,
        ticket_url: gen_ticket_client_url_with_auth_token(organization, user_id, ticket.number)
      })
      |> Mailer.deliver_later()

      {:ok, :sent}
    else
      _ ->
        {:ok, :not_sent}
    end
  end

  def maybe_send_ticket_replay_to_client_email(ticket, ticket_message) do
    with %{contact_id: contact_id, organization_id: organization_id} when not is_nil(contact_id) <-
           ticket,
         {:ok, %{id: user_id, email: email}} <-
           Accounts.get_user_by_user_organization_id(contact_id),
         {:ok, organization} <-
           Accounts.get_organization_by_id(organization_id) do
      subject = "Re: Ticket ##{ticket.number} - #{ticket.subject}"
      from = Application.get_env(:micro_chat, MicroChat.Mailer)[:default_from_address]

      Email.ticket_update_to_client(from, email, subject, %{
        ticket_message: ticket_message,
        ticket_number: ticket.number,
        ticket_url: gen_ticket_client_url_with_auth_token(organization, user_id, ticket.number)
      })
      |> Mailer.deliver_later()

      {:ok, :sent}
    else
      _ ->
        {:ok, :not_sent}
    end
  end

  def gen_ticket_admin_url(ticket_id) do
    admin_url = Application.get_env(:micro_chat, MicroChatWeb.Endpoint)[:admin_url]

    "#{admin_url}/home/tickets/#{ticket_id}"
  end

  def gen_ticket_client_url(organization, ticket_id) do
    client_url = OrganizationHelper.get_organization_client_url(organization)

    "#{client_url}/home/tickets/#{ticket_id}"
  end

  def gen_ticket_client_url_with_auth_token(organization, user_id, ticket_number) do
    ticket_url = gen_ticket_client_url(organization, ticket_number)

    auth_token =
      TokenHelper.sign_tenant_data(MicroChatWeb.Endpoint, %{
        user_id: user_id,
        organization_id: organization.id
      })

    "#{ticket_url}?auth_token=#{auth_token}"
  end
end
