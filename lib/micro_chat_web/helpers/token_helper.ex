defmodule MicroChatWeb.TokenHelper do
  @auth_websocket_namespace "websocket auth"
  @auth_tenant_namespace "tenant auth"

  def sign_websocket_data(context, data) do
    Phoenix.Token.sign(context, @auth_websocket_namespace, data)
  end

  def verify_websocket_token(context, token) do
    Phoenix.Token.verify(context, @auth_websocket_namespace, token, max_age: 1_209_600)
  end

  def sign_tenant_data(context, data) do
    Phoenix.Token.sign(context, @auth_tenant_namespace, data)
  end

  def verify_tenant_token(context, token, opts \\ []) do
    Phoenix.Token.verify(context, @auth_tenant_namespace, token, opts)
  end
end
