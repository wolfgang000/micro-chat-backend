defmodule MicroChatWeb.CookieAuthPlug do
  import Plug.Conn
  alias MicroChat.Accounts

  @doc false
  def init(default), do: default

  @doc false
  def call(%Plug.Conn{} = conn, _options) do
    with current_session_id when not is_nil(current_session_id) <-
           get_session(conn, :current_session_id),
         {:ok,
          %{
            user_organization:
              %{
                id: user_organization_id,
                user_id: user_id,
                organization_id: organization_id,
                role_id: role_id
              } = user_organization
          }} <-
           Accounts.get_session_by_id_with_preloaded_data(current_session_id) do
      conn
      |> assign(:current_user_id, user_id)
      |> assign(:current_organization_id, organization_id)
      |> assign(:current_user_organization_id, user_organization_id)
      |> assign(:current_user_organization, user_organization)
      |> assign(:current_role_id, role_id)
      |> assign(:current_session_id, current_session_id)
    else
      _ -> unauthorized(conn)
    end
  end

  defp unauthorized(conn) do
    conn
    |> send_resp(401, "")
    |> halt()
  end
end
