defmodule MicroChatWeb.AdminRolesAuthPlug do
  import Plug.Conn
  alias MicroChat.Accounts.Role

  @role_id_agent Role.role_id_agent()
  @role_id_admin Role.role_id_admin()
  @role_id_owner Role.role_id_owner()

  @admin_roles [@role_id_agent, @role_id_admin, @role_id_owner]

  @doc false
  def init(default), do: default

  @doc false
  def call(%Plug.Conn{assigns: %{current_role_id: role_id}} = conn, _options)
      when role_id in @admin_roles do
    conn
  end

  def call(conn, _options), do: forbidden(conn)

  defp forbidden(conn) do
    conn
    |> send_resp(403, "")
    |> halt()
  end
end
