defmodule MicroChatWeb.OrganizationSubdomainPlug do
  import Plug.Conn
  alias MicroChat.Accounts

  @doc false
  def init(default), do: default

  @doc false
  def call(%Plug.Conn{} = conn, _default) do
    with subdomain when byte_size(subdomain) > 0 <- get_subdomain(conn.host),
         {:ok, organization} <- Accounts.get_organization_by_id(subdomain) do
      conn
      |> assign(:current_tenant_organization, organization)
    else
      _ -> not_found(conn)
    end
  end

  defp get_client_root_host,
    do: Application.get_env(:micro_chat, MicroChatWeb.Endpoint)[:client_root_host]

  defp get_subdomain(host) do
    root_host = get_client_root_host()
    String.replace(host, ~r/.?#{root_host}/, "")
  end

  defp not_found(conn) do
    conn
    |> Plug.Conn.send_resp(404, "Organization not found")
    |> halt()
  end
end
