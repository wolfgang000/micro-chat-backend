defmodule MicroChat.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      MicroChat.Repo,
      # Start the Telemetry supervisor
      MicroChatWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: MicroChat.PubSub},
      # Start the Endpoint (http/https)
      MicroChatWeb.Endpoint,
      # Start a worker by calling: MicroChat.Worker.start_link(arg)
      # {MicroChat.Worker, arg}
      MicroChat.Presence
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: MicroChat.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    MicroChatWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
