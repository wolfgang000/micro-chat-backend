defmodule MicroChat.Chat.Ticket do
  use Ecto.Schema
  import Ecto.Changeset

  @status_closed 1
  def status_closed, do: @status_closed

  @status_open 2
  def status_open, do: @status_open

  @tickets_statuses %{
    status_closed: @status_closed,
    status_open: @status_open
  }

  schema "tickets" do
    field :number, :integer
    field :subject, :string
    field :status, :integer
    belongs_to :assigned_agent, MicroChat.Accounts.UserOrganization
    belongs_to :contact, MicroChat.Accounts.UserOrganization
    belongs_to :organization, MicroChat.Accounts.Organization
    timestamps(type: :utc_datetime)
  end

  @required_attrs [
    :contact_id,
    :organization_id,
    :subject,
    :status
  ]

  @attrs @required_attrs ++ [:number, :assigned_agent_id]
  @doc false
  def changeset(ticket, attrs) do
    ticket
    |> cast(attrs, @attrs)
    |> validate_required(@required_attrs)
    |> foreign_key_constraint(:organization_id)
    |> foreign_key_constraint(:assigned_agent_id)
    |> foreign_key_constraint(:contact_id)
    |> validate_inclusion(:status, Map.values(@tickets_statuses))
  end

  def update_changeset(ticket, attrs) do
    ticket
    |> cast(attrs, [:status, :assigned_agent_id])
    |> validate_required(@required_attrs)
    |> foreign_key_constraint(:organization_id)
    |> foreign_key_constraint(:assigned_agent_id)
    |> foreign_key_constraint(:contact_id)
    |> validate_inclusion(:status, Map.values(@tickets_statuses))
  end
end
