defmodule MicroChat.Chat.Message do
  use Ecto.Schema
  import Ecto.Changeset

  schema "messages" do
    field :content, :string
    belongs_to :user_organization, MicroChat.Accounts.UserOrganization
    belongs_to :ticket, MicroChat.Chat.Ticket
    timestamps(type: :utc_datetime)
  end

  @required_attrs [
    :content,
    :user_organization_id,
    :ticket_id
  ]

  @attrs @required_attrs
  @doc false
  def changeset(message, attrs) do
    message
    |> cast(attrs, @attrs)
    |> validate_required(@required_attrs)
    |> validate_length(:content, max: 12_000)
    |> foreign_key_constraint(:ticket_id)
    |> foreign_key_constraint(:user_id)
  end
end
