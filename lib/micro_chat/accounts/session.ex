defmodule MicroChat.Accounts.Session do
  use Ecto.Schema
  import Ecto.Changeset

  # TODO: Add seesion_key(maybe hashed), is_staff, expiration_date
  schema "sessions" do
    belongs_to(:user_organization, MicroChat.Accounts.UserOrganization)
    timestamps(type: :utc_datetime)
  end

  @required_attrs [
    :user_organization_id
  ]

  @attrs @required_attrs

  @doc false
  def changeset(sessions, attrs) do
    sessions
    |> cast(attrs, @attrs)
    |> validate_required(@required_attrs)
    |> foreign_key_constraint(:user_organization_id)
  end
end
