defmodule MicroChat.Accounts.UserOrganization do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users_organizations" do
    belongs_to :user, MicroChat.Accounts.User
    belongs_to :organization, MicroChat.Accounts.Organization
    belongs_to :role, MicroChat.Accounts.Role
    timestamps(type: :utc_datetime)
  end

  @required_attrs [
    :user_id,
    :organization_id,
    :role_id
  ]

  @attrs @required_attrs

  @doc false
  def changeset(user_organization, attrs) do
    user_organization
    |> cast(attrs, @attrs)
    |> validate_required(@required_attrs)
    |> foreign_key_constraint(:user_id)
    |> foreign_key_constraint(:organization_id)
    |> foreign_key_constraint(:role_id)
    |> unique_constraint([:user_id, :organization_id])
  end
end
