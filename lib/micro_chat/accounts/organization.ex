defmodule MicroChat.Accounts.Organization do
  use Ecto.Schema
  import Ecto.Changeset

  schema "organizations" do
    field :name, :string

    has_many :user_organizations, MicroChat.Accounts.UserOrganization
    timestamps(type: :utc_datetime)
  end

  @required_attrs [
    :name
  ]

  @attrs @required_attrs

  @doc false
  def changeset(organization, attrs) do
    organization
    |> cast(attrs, @attrs)
    |> validate_required(@required_attrs)
    |> unique_constraint(:uuid)
  end
end
