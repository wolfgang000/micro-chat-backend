defmodule MicroChat.Accounts.Role do
  use Ecto.Schema
  import Ecto.Changeset

  schema "roles" do
    field :name, :string
  end

  @role_id_owner 1
  def role_id_owner, do: @role_id_owner

  @role_id_admin 2
  def role_id_admin, do: @role_id_admin

  @role_id_agent 3
  def role_id_agent, do: @role_id_agent

  @role_id_contact 4
  def role_id_contact, do: @role_id_contact

  @role_ids %{
    owner: @role_id_owner,
    admin: @role_id_admin,
    agent: @role_id_agent,
    contact: @role_id_contact
  }
  def role_ids, do: @role_ids

  @staff_role_ids [@role_id_owner, @role_id_admin, @role_id_agent]
  def staff_role_ids, do: @staff_role_ids

  @doc false
  def changeset(role, attrs) do
    role
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
