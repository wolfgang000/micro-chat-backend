defmodule MicroChat.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :email, :string
    field :name, :string
    field :password, :string, virtual: true
    field :password_hash, :string
    field :last_login, :utc_datetime
    field :is_admin_account_created, :boolean

    timestamps(type: :utc_datetime)
  end

  @required_attrs [
    :email,
    :is_admin_account_created
  ]

  @attrs @required_attrs ++ [:name, :last_login]

  def changeset(user, attrs) do
    user
    |> cast(attrs, @attrs)
    |> validate_required(@required_attrs)
    |> downcase_email()
  end

  @doc false
  def registration_changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :password, :name, :is_admin_account_created])
    |> validate_required([:email, :password, :name, :is_admin_account_created])
    |> validate_length(:password, min: 7)
    |> validate_format(:email, ~r/@/)
    |> downcase_email()
    |> unique_constraint(:email)
    |> put_password_hash()
  end

  defp downcase_email(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{email: email}} ->
        put_change(
          changeset,
          :email,
          email
          |> String.downcase()
          |> String.trim()
        )

      _ ->
        changeset
    end
  end

  defp put_password_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
        put_change(changeset, :password_hash, Argon2.hash_pwd_salt(pass))

      _ ->
        changeset
    end
  end
end
