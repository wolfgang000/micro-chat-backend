defmodule MicroChat.Accounts.TicketOrganizationSeqTracker do
  use Ecto.Schema
  import Ecto.Changeset

  schema "ticket_organization_seq_tracker" do
    belongs_to :organization, MicroChat.Accounts.Organization
    field :last_id, :integer
  end

  @required_attrs [
    :organization_id,
    :last_id
  ]

  @attrs @required_attrs

  def changeset(user, attrs) do
    user
    |> cast(attrs, @attrs)
    |> validate_required(@required_attrs)
  end
end
