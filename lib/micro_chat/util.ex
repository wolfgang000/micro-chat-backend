defmodule MicroChat.Util do
  def return_value_or_not_found(v) do
    case v do
      nil -> {:error, :not_found}
      value -> {:ok, value}
    end
  end
end
