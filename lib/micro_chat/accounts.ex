defmodule MicroChat.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias MicroChat.{Repo, Util}
  alias Ecto.Multi

  alias MicroChat.Accounts.{
    User,
    Organization,
    UserOrganization,
    Session,
    Role,
    TicketOrganizationSeqTracker
  }

  @role_staff_ids Role.staff_role_ids()

  @role_contact_id Role.role_id_contact()

  def list_users do
    Repo.all(User)
  end

  def get_user!(id), do: Repo.get!(User, id)

  def get_user_by_id(id) do
    Repo.get(User, id)
    |> Util.return_value_or_not_found()
  end

  def get_user_by_email(email),
    do: Repo.get_by(User, email: email) |> Util.return_value_or_not_found()

  def get_user_by_user_organization_id(user_organization_id) do
    from(u in User,
      join: uo in UserOrganization,
      on: uo.user_id == u.id,
      where: uo.id == ^user_organization_id,
      limit: 1
    )
    |> Repo.one()
    |> Util.return_value_or_not_found()
  end

  def get_user_organization_by_user_id_and_organization_id(user_id, organization_id) do
    from(uo in UserOrganization,
      where: uo.user_id == ^user_id,
      where: uo.organization_id == ^organization_id,
      limit: 1
    )
    |> Repo.one()
    |> Util.return_value_or_not_found()
  end

  def get_organization_by_id(id),
    do: Repo.get(Organization, id) |> Util.return_value_or_not_found()

  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  def change_user(%User{} = user, attrs \\ %{}) do
    User.changeset(user, attrs)
  end

  def get_organization!(id), do: Repo.get!(Organization, id)

  def get_organization_by_uuid(uuid) do
    Repo.get_by(Organization, uuid: uuid)
    |> case do
      nil -> {:error, :not_found}
      value -> {:ok, value}
    end
  end

  def create_organization(attrs \\ %{}) do
    %Organization{}
    |> Organization.changeset(attrs)
    |> Repo.insert()
  end

  def update_organization(%Organization{} = organization, attrs) do
    organization
    |> Organization.changeset(attrs)
    |> Repo.update()
  end

  def delete_organization(%Organization{} = organization) do
    Repo.delete(organization)
  end

  def change_organization(%Organization{} = organization, attrs \\ %{}) do
    Organization.changeset(organization, attrs)
  end

  alias MicroChat.Accounts.UserOrganization

  def list_users_organizations do
    Repo.all(UserOrganization)
  end

  def list_users_organizations(organization_id, params) do
    from(uo in UserOrganization,
      as: :user_organization,
      join: user in assoc(uo, :user),
      preload: [user: user],
      where: uo.organization_id == ^organization_id
    )
    |> filter_by_organization_id(params)
    |> Repo.all()
  end

  defp filter_by_organization_id(query, %{"filter" => "staff_only"}) do
    from([user_organization: uo] in query, where: uo.role_id in @role_staff_ids)
  end

  defp filter_by_organization_id(query, %{"filter" => "contacts_only"}) do
    from([user_organization: uo] in query, where: uo.role_id == @role_contact_id)
  end

  defp filter_by_organization_id(query, _params), do: query

  def get_user_organization!(id), do: Repo.get!(UserOrganization, id)

  def create_user_organization(attrs \\ %{}) do
    %UserOrganization{}
    |> UserOrganization.changeset(attrs)
    |> Repo.insert()
  end

  def get_first_user_organization(user_id) do
    from(uo in UserOrganization,
      where: uo.user_id == ^user_id,
      limit: 1
    )
    |> Repo.one()
    |> case do
      nil -> {:error, :not_found}
      user_organization -> {:ok, user_organization}
    end
  end

  def get_user_organization(user_id, organization_id) do
    from(uo in UserOrganization,
      where: uo.user_id == ^user_id,
      where: uo.organization_id == ^organization_id
    )
    |> Repo.one()
    |> Util.return_value_or_not_found()
  end

  def get_user_organization_with_user_by_id(user_organization_id) do
    from(uo in UserOrganization,
      join: user in assoc(uo, :user),
      where: uo.id == ^user_organization_id,
      preload: [user: user]
    )
    |> Repo.one()
    |> Util.return_value_or_not_found()
  end

  def update_user_organization(%UserOrganization{} = user_organization, attrs) do
    user_organization
    |> UserOrganization.changeset(attrs)
    |> Repo.update()
  end

  def delete_user_organization(%UserOrganization{} = user_organization) do
    Repo.delete(user_organization)
  end

  def change_user_organization(%UserOrganization{} = user_organization, attrs \\ %{}) do
    UserOrganization.changeset(user_organization, attrs)
  end

  def authenticate_user(email, plain_text_password) do
    query = from u in User, where: u.email == ^email

    Repo.one(query)
    |> check_password(plain_text_password)
  end

  defp check_password(nil, _) do
    {:error,
     Ecto.Changeset.change({%{}, %{email: :string}}, %{email: ""})
     |> Ecto.Changeset.add_error(:email, "wrong email or password")}
  end

  defp check_password(user, plain_text_password) do
    if Argon2.verify_pass(plain_text_password, user.password_hash) do
      {:ok, user}
    else
      {:error,
       Ecto.Changeset.change({%{}, %{email: :string}}, %{email: user.email})
       |> Ecto.Changeset.add_error(:email, "wrong email or password")}
    end
  end

  def update_user_last_login_date(user) do
    update_user(user, %{last_login: DateTime.utc_now()})
  end

  def register_user(attrs \\ %{}) do
    %User{}
    |> User.registration_changeset(attrs)
    |> Repo.insert()
  end

  def register_user_multi(multi, user_params) do
    multi
    |> Multi.run(:create_user, fn repo, _changes ->
      %User{}
      |> User.registration_changeset(user_params)
      |> repo.insert()
    end)
  end

  def create_organization_multi(multi, %{"name" => _} = organization_params) do
    multi
    |> Multi.run(:get_or_create_organization, fn repo, _changes ->
      %Organization{}
      |> Organization.changeset(organization_params)
      |> repo.insert()
    end)
    |> Multi.run(:create_ticket_organization_seq_tracker, fn repo,
                                                             %{
                                                               get_or_create_organization:
                                                                 organization
                                                             } ->
      %TicketOrganizationSeqTracker{}
      |> TicketOrganizationSeqTracker.changeset(%{organization_id: organization.id, last_id: 0})
      |> repo.insert()
    end)
  end

  def get_organization_multi(multi, %{"id" => organization_id} = _organization_params) do
    multi
    |> Multi.run(:get_or_create_organization, fn repo, _changes ->
      repo.get(Organization, organization_id)
      |> Util.return_value_or_not_found()
    end)
  end

  def assoc_user_organization_multi(multi) do
    multi
    |> Multi.run(:assoc_user_organization, fn repo,
                                              %{
                                                create_user: user,
                                                get_or_create_organization: organization
                                              } ->
      %UserOrganization{}
      |> UserOrganization.changeset(%{
        "user_id" => user.id,
        "organization_id" => organization.id,
        "role_id" => Role.role_id_owner()
      })
      |> repo.insert()
    end)
  end

  def create_user_with_organization(user_params, organization_params) do
    Multi.new()
    |> register_user_multi(user_params)
    |> create_organization_multi(organization_params)
    |> assoc_user_organization_multi()
    |> Repo.transaction()
    # TODO: Refactor
    |> case do
      {:ok, %{create_user: user, get_or_create_organization: organization}} ->
        {:ok,
         %{
           user: user,
           organization: organization
         }}

      {:error, _step, error, _changes} ->
        {:error, error}
    end
  end

  def create_user_and_assoc_user_to_organization(user_params, organization_params) do
    Multi.new()
    |> register_user_multi(user_params)
    |> get_organization_multi(organization_params)
    |> assoc_user_organization_multi()
    |> Repo.transaction()
    # TODO: Refactor
    |> case do
      {:ok, %{create_user: user, get_or_create_organization: organization}} ->
        {:ok,
         %{
           user: user,
           organization: organization
         }}

      {:error, _step, error, _changes} ->
        {:error, error}
    end
  end

  def get_session_by_id(id) do
    Repo.get(Session, id)
    |> Util.return_value_or_not_found()
  end

  def delete_session_by_id(session_id) do
    Repo.get!(Session, session_id)
    |> Repo.delete()
  end

  def get_session_by_id_with_preloaded_data(session_id) do
    from(session in Session,
      where: session.id == ^session_id,
      join: user_organization in assoc(session, :user_organization),
      preload: [user_organization: user_organization]
    )
    |> Repo.one()
    |> Util.return_value_or_not_found()
  end

  def create_session(attrs \\ %{}) do
    %Session{}
    |> Session.changeset(attrs)
    |> Repo.insert()
  end

  def get_or_create_user_multi(multi, %{"id" => user_id} = _user_params) do
    multi
    |> Multi.run(:get_or_create_user, fn repo, _changes ->
      repo.get(User, user_id)
      |> Util.return_value_or_not_found()
    end)
  end

  def get_or_create_user_multi(multi, %{"email" => email} = _user_params) do
    email =
      email
      |> String.downcase()
      |> String.trim()

    user_resp = get_user_by_email(email)

    multi
    |> Multi.run(:get_or_create_user, fn repo, _changes ->
      case user_resp do
        {:ok, _} ->
          user_resp

        _ ->
          %User{}
          |> User.changeset(%{
            "email" => email,
            "is_admin_account_created" => false
          })
          |> repo.insert()
      end
    end)
  end

  def get_or_create_user_organization_multi(multi, organization_id, default_role_id) do
    multi
    |> Multi.run(:get_or_create_user_organization, fn repo, %{get_or_create_user: user} ->
      get_user_organization_by_user_id_and_organization_id(user.id, organization_id)
      |> case do
        {:ok, _} = resp ->
          resp

        _ ->
          %UserOrganization{}
          |> UserOrganization.changeset(%{
            "user_id" => user.id,
            "organization_id" => organization_id,
            "role_id" => default_role_id
          })
          |> repo.insert()
      end
    end)
  end
end
