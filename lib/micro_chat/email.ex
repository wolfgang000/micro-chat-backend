defmodule MicroChat.Email do
  use Bamboo.Phoenix, view: MicroChat.EmailView

  def template_email(to, html_body) do
    new_email(
      to: to,
      from: "support@myapp.com",
      subject: "Welcome to the app.",
      html_body: html_body
    )
  end

  def sign_in_email(person) do
    base_email()
    |> to(person)
    |> subject("Your Sign In Link")
    |> assign(:person, person)
    |> render("sign_in.html")
  end

  defp base_email do
    new_email()
    |> put_html_layout({MicroChatWeb.LayoutView, "email.html"})
    |> put_text_layout({MicroChatWeb.LayoutView, "email.text"})
  end

  def ticket_assigned_email(from, recipient, subject, params) do
    base_email()
    |> from(from)
    |> to(recipient)
    |> subject(subject)
    |> render(:ticket_assigned, params)
  end

  def ticket_update_to_client(from, recipient, subject, params) do
    base_email()
    |> from(from)
    |> to(recipient)
    |> subject(subject)
    |> render(:ticket_update_to_client, params)
  end
end
