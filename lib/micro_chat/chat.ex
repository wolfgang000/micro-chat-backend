defmodule MicroChat.Chat do
  @moduledoc """
  The Core context.
  """

  import Ecto.Query, warn: false
  alias MicroChat.{Repo, Util, Accounts}
  alias MicroChat.Chat.{Ticket, Message}
  alias MicroChat.Accounts.Role
  alias Ecto.Multi

  def get_ticket_by_id(id) do
    Repo.get(Ticket, id)
    |> Util.return_value_or_not_found()
  end

  def get_ticket(ticket_number, organization_id) do
    from(t in Ticket,
      join: contact in assoc(t, :contact),
      join: contact_user in assoc(contact, :user),
      preload: [contact: {contact, user: contact_user}],
      where: t.number == ^ticket_number,
      where: t.organization_id == ^organization_id
    )
    |> Repo.one()
    |> Util.return_value_or_not_found()
  end

  def list_organizations_tickets(organization_id) do
    from(t in Ticket,
      join: contact in assoc(t, :contact),
      join: contact_user in assoc(contact, :user),
      preload: [contact: {contact, user: contact_user}],
      where: t.organization_id == ^organization_id
    )
    |> Repo.all()
  end

  def create_ticket(attrs \\ %{}) do
    %Ticket{}
    |> Ticket.changeset(attrs)
    |> Repo.insert()
  end

  def update_ticket(%Ticket{} = ticket, attrs) do
    ticket
    |> Ticket.update_changeset(attrs)
    |> Repo.update()
  end

  def create_message(attrs \\ %{}) do
    %Message{}
    |> Message.changeset(attrs)
    |> Repo.insert()
  end

  def create_ticket_with_message(ticket_params, message_params) do
    Multi.new()
    |> get_or_create_contact(ticket_params)
    |> create_ticket_multi(ticket_params)
    |> create_message_multi(message_params)
    |> Repo.transaction()
    # TODO: Refactor
    |> case do
      {:error, _step, error, _changes} ->
        {:error, error}

      resp ->
        resp
    end
  end

  defp create_ticket_multi(multi, attrs) do
    multi
    |> Multi.run(:create_ticket, fn repo, %{get_or_create_contact: contact} ->
      %Ticket{}
      |> Ticket.changeset(attrs |> Map.put("contact_id", contact.id))
      |> repo.insert()
    end)
  end

  defp create_message_multi(multi, attrs) do
    multi
    |> Multi.run(:create_message, fn repo, %{create_ticket: %{id: ticket_id}} ->
      %Message{}
      |> Message.changeset(attrs |> Map.put("ticket_id", ticket_id))
      |> repo.insert()
    end)
  end

  defp get_or_create_contact(multi, %{
         "organization_id" => organization_id,
         "contact" => %{"email" => email}
       }) do
    multi
    |> Accounts.get_or_create_user_multi(%{"email" => email})
    |> Accounts.get_or_create_user_organization_multi(organization_id, Role.role_id_contact())
    |> Multi.run(:get_or_create_contact, fn _repo,
                                            %{get_or_create_user_organization: user_organization} ->
      {:ok, user_organization}
    end)
  end

  def get_messages_by_ticket(ticket_number, organization_id),
    do:
      Repo.all(
        from m in Message,
          join: ticket in assoc(m, :ticket),
          where: ticket.number == ^ticket_number,
          where: ticket.organization_id == ^organization_id,
          order_by: [desc: m.inserted_at]
      )
end
