defmodule MicroChatWeb.BrowserChannelTest do
  use MicroChatWeb.ChannelCase
  import MicroChat.Factory
  alias MicroChatWeb.TokenHelper

  describe "authentication" do
    test "with valid token" do
      %{
        id: session_id,
        user_organization: %{
          id: user_organization_id,
          user_id: user_id,
          organization_id: organization_id
        }
      } = session = insert(:session)

      token = TokenHelper.sign_websocket_data(@endpoint, session.id)
      assert {:ok, socket} = connect(MicroChatWeb.UserSocket, %{"token" => token})
      assert socket.assigns.current_session_id == session_id
      assert socket.assigns.current_user_id == user_id
      assert socket.assigns.current_organization_id == organization_id
      assert socket.assigns.current_user_organization.id == user_organization_id
    end

    test "with invalid token" do
      assert :error = connect(MicroChatWeb.UserSocket, %{"token" => "invalid-token"})
    end

    test "without token" do
      assert :error = connect(MicroChatWeb.UserSocket, %{})
    end
  end
end
