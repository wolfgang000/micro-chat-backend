defmodule MicroChatWeb.TicketChannelTest do
  use MicroChatWeb.ChannelCase
  import MicroChat.Factory
  alias MicroChat.TestHelper
  use Bamboo.Test, shared: true

  def gen_authenticated_socket(_) do
    TestHelper.gen_authenticated_admin_socket()
  end

  def subscribe_socket_to_channel(%{socket: socket}) do
    %{contact: contact} = ticket = insert(:ticket)

    {:ok, _, subscribed_socket} =
      socket
      |> subscribe_and_join(MicroChatWeb.TicketChannel, "ticket:#{ticket.id}")

    {:ok, %{subscribed_socket: subscribed_socket, ticket: ticket, contact: contact}}
  end

  describe "try joining channel" do
    setup [:gen_authenticated_socket]

    test "with valid ticket id", %{socket: socket} do
      ticket = insert(:ticket)

      {:ok, _, _socket} =
        socket
        |> subscribe_and_join(MicroChatWeb.TicketChannel, "ticket:#{ticket.id}")
    end

    test "with invalid ticket id", %{socket: socket} do
      {:error, _} =
        socket
        |> subscribe_and_join(MicroChatWeb.TicketChannel, "ticket:9999")
    end
  end

  describe "test channel" do
    setup [:gen_authenticated_socket, :subscribe_socket_to_channel]

    test "ping replies with status ok", %{subscribed_socket: socket} do
      ref = push(socket, "ping", %{"hello" => "there"})
      assert_reply ref, :ok, %{"hello" => "there"}
    end

    test "shout broadcasts to ticket:lobby", %{subscribed_socket: socket} do
      push(socket, "shout", %{"hello" => "all"})
      assert_broadcast "shout", %{"hello" => "all"}
    end

    test "broadcasts are pushed to the client", %{subscribed_socket: socket} do
      broadcast_from!(socket, "broadcast", %{"some" => "data"})
      assert_push "broadcast", %{"some" => "data"}
    end

    test "create new message, reply to client", %{
      subscribed_socket: socket,
      ticket: _ticket,
      contact: contact
    } do
      ref = push(socket, "message:create", %{"content" => "testing text"})
      assert_reply ref, :ok, %{id: message_id, content: "testing text"}
      assert_broadcast "message:created", %{id: ^message_id, content: "testing text"}

      assert_email_delivered_with(subject: ~r/Re: Ticket/, to: [contact.user.email])
    end
  end
end
