defmodule MicroChatWeb.UserControllerTest do
  use MicroChatWeb.ConnCase
  use Bamboo.Test
  import MicroChat.Factory
  alias MicroChat.TestHelper

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  defp create_authenticated_conn(%{conn: conn}) do
    TestHelper.gen_authenticated_admin_conn(conn)
  end

  describe "index" do
    setup [:create_authenticated_conn]

    test "lists organization's staff users", %{conn: conn, organization: organization} do
      _agent_user =
        insert(:user_organization,
          organization: organization,
          role: nil,
          role_id: MicroChat.Accounts.Role.role_id_agent()
        )

      _contact_user =
        insert(:user_organization,
          organization: organization,
          role: nil,
          role_id: MicroChat.Accounts.Role.role_id_contact()
        )

      conn = get(conn, Routes.user_path(conn, :index), %{filter: "staff_only"})
      assert [user1, user2] = json_response(conn, 200)["data"]
      refute user1["role_id"] == MicroChat.Accounts.Role.role_id_contact()
      refute user2["role_id"] == MicroChat.Accounts.Role.role_id_contact()
    end

    test "lists organization's contacts users", %{conn: conn, organization: organization} do
      _agent_user =
        insert(:user_organization,
          organization: organization,
          role: nil,
          role_id: MicroChat.Accounts.Role.role_id_agent()
        )

      %{user: %{id: user_id}} =
        _contact_user =
        insert(:user_organization,
          organization: organization,
          role: nil,
          role_id: MicroChat.Accounts.Role.role_id_contact()
        )

      conn = get(conn, Routes.user_path(conn, :index), %{filter: "contacts_only"})
      assert [user] = json_response(conn, 200)["data"]
      assert user["role_id"] == MicroChat.Accounts.Role.role_id_contact()
      assert user["user"]["id"] == user_id
    end

    # test "don't lists other organization's tickets", %{conn: conn} do
    #   insert(:ticket)
    #   conn = get(conn, Routes.ticket_path(conn, :index))
    #   assert [_ticket] = json_response(conn, 200)["data"]
    # end
  end

  # describe "show" do
  #   setup [:create_authenticated_conn, :create_ticket]

  #   test "show organization's ticket", %{conn: conn, ticket: %{id: ticket_id}} do
  #     conn = get(conn, Routes.ticket_path(conn, :show, ticket_id))
  #     assert resp_ticket = json_response(conn, 200)["data"]
  #     assert %{"id" => ^ticket_id} = resp_ticket
  #   end

  #   test "don't show other organization's ticket", %{conn: conn} do
  #     ticket = insert(:ticket)
  #     conn = get(conn, Routes.ticket_path(conn, :show, ticket.id))
  #     assert _resp_ticket = json_response(conn, 404)["errors"]
  #   end
  # end

  # describe "create ticket" do
  #   setup [:create_authenticated_conn]

  #   test "renders ticket when data is valid", %{conn: conn} do
  #     agent = insert(:user_organization)

  #     conn =
  #       post(conn, Routes.ticket_path(conn, :create),
  #         ticket: %{
  #           subject: "testing-subject",
  #           assigned_agent_id: agent.id,
  #           contact: %{
  #             email: "contact@mail.com"
  #           }
  #         },
  #         message: %{
  #           content: "testing-message"
  #         }
  #       )

  #     assert %{"id" => id} = json_response(conn, 201)["data"]

  #     assert_email_delivered_with(
  #       subject: ~r/testing-subject/,
  #       html_body: ~r/testing-message/,
  #       text_body: ~r/home\/tickets\//
  #     )

  #     conn = get(conn, Routes.ticket_path(conn, :show, id))

  #     assert %{
  #              "id" => id
  #            } = json_response(conn, 200)["data"]
  #   end

  #   test "try create with invalid data", %{conn: conn} do
  #     conn = post(conn, Routes.ticket_path(conn, :create), ticket: nil)
  #     assert response(conn, 400)
  #   end
  # end

  # describe "update ticket" do
  #   setup [:create_ticket]

  #   test "renders ticket when data is valid", %{conn: conn, ticket: %Ticket{id: id} = ticket} do
  #     conn = put(conn, Routes.ticket_path(conn, :update, ticket), ticket: @update_attrs)
  #     assert %{"id" => ^id} = json_response(conn, 200)["data"]

  #     conn = get(conn, Routes.ticket_path(conn, :show, id))

  #     assert %{
  #              "id" => id
  #            } = json_response(conn, 200)["data"]
  #   end

  #   test "renders errors when data is invalid", %{conn: conn, ticket: ticket} do
  #     conn = put(conn, Routes.ticket_path(conn, :update, ticket), ticket: @invalid_attrs)
  #     assert json_response(conn, 422)["errors"] != %{}
  #   end
  # end

  # describe "delete ticket" do
  #   setup [:create_ticket]

  #   test "deletes chosen ticket", %{conn: conn, ticket: ticket} do
  #     conn = delete(conn, Routes.ticket_path(conn, :delete, ticket))
  #     assert response(conn, 204)

  #     assert_error_sent 404, fn ->
  #       get(conn, Routes.ticket_path(conn, :show, ticket))
  #     end
  #   end
  # end
end
