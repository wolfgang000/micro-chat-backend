defmodule MicroChatWeb.AccountControllerTest do
  use MicroChatWeb.ConnCase
  import MicroChat.Factory
  alias MicroChat.{Accounts, TestHelper}
  alias MicroChatWeb.TokenHelper

  @valid_attrs %{
    name: "waldo garcia",
    email: "test@mail.com",
    password: "some password",
    is_admin_account_created: true
  }
  @valid_login_attrs %{email: "test@mail.com", password: "some password"}
  @invalid_login_attrs %{email: "test@mail.com", password: "some wrong password"}

  def fixture(:user) do
    {:ok, user} = Accounts.register_user(@valid_attrs)
    user
  end

  defp create_user(_) do
    user = fixture(:user)
    %{organization: organization} = user_organization = insert(:user_organization, user: user)

    {:ok, user: user, organization: organization, user_organization: user_organization}
  end

  defp create_tenant_organization_conn(%{conn: conn, organization: organization}) do
    TestHelper.gen_tenant_organization_conn(conn, "#{organization.id}")
  end

  describe "login" do
    setup [:create_user]

    test "with invalid credentials, wrong password", %{conn: conn} do
      conn = post(conn, Routes.account_path(conn, :login), user: @invalid_login_attrs)
      response_data = json_response(conn, 422)["errors"]
      assert %{"email" => email_errors} = response_data
    end

    test "with invalid credentials, wrong email", %{conn: conn} do
      conn =
        post(conn, Routes.account_path(conn, :login),
          user: %{email: "test123@mail.com", password: "some wrong password"}
        )

      response_data = json_response(conn, 422)["errors"]
      assert %{"email" => email_errors} = response_data
    end

    test "with valid credentials", %{
      conn: conn,
      user_organization: %{id: user_organization_id}
    } do
      conn = post(conn, Routes.account_path(conn, :login), user: @valid_login_attrs)
      response_data = json_response(conn, 200)["data"]

      assert %{
               "email" => "test@mail.com",
               "name" => "waldo garcia",
               "user_organization_id" => ^user_organization_id
             } = response_data

      assert %{"current_session_id" => _} = get_session(conn)
    end

    test "with upcase email", %{conn: conn} do
      conn =
        post(conn, Routes.account_path(conn, :login),
          user: %{@valid_attrs | email: String.upcase(@valid_attrs.email)}
        )

      assert conn.status == 200
    end

    test "check valid login's last date", %{conn: conn, user: user} do
      conn = post(conn, Routes.account_path(conn, :login), user: @valid_attrs)
      assert conn.status == 200
      logged_user = Accounts.get_user!(user.id)
      assert DateTime.diff(DateTime.utc_now(), logged_user.last_login, :millisecond) < 1500
    end
  end

  describe "login tenant" do
    setup [:create_user, :create_tenant_organization_conn]

    test "with valid credentials", %{
      conn: conn,
      user_organization: %{id: user_organization_id}
    } do
      conn =
        post(conn, Routes.account_path(conn, :login_client),
          user: %{email: "test@mail.com", code: "some_code"}
        )

      response_data = json_response(conn, 200)["data"]

      assert %{
               "email" => "test@mail.com",
               "name" => "waldo garcia",
               "user_organization_id" => ^user_organization_id
             } = response_data

      assert %{"current_session_id" => _} = get_session(conn)
    end

    test "with token", %{
      conn: conn,
      user_organization: %{
        id: user_organization_id,
        user_id: user_id,
        organization_id: organization_id
      }
    } do
      token =
        TokenHelper.sign_tenant_data(@endpoint, %{
          user_id: user_id,
          organization_id: organization_id
        })

      conn = post(conn, Routes.account_path(conn, :login_client), user: %{token: token})

      response_data = json_response(conn, 200)["data"]

      assert %{
               "email" => "test@mail.com",
               "name" => "waldo garcia",
               "user_organization_id" => ^user_organization_id
             } = response_data

      assert %{"current_session_id" => _} = get_session(conn)
    end
  end

  describe "signup" do
    test "and create organization with valid params", %{conn: conn} do
      params = %{
        user: %{
          name: "walter white",
          email: "test@win.com",
          password: "abcpassword"
        },
        organization: %{
          name: "test123"
        }
      }

      conn = post(conn, Routes.account_path(conn, :signup), params)
      response_data = json_response(conn, 201)
      assert response_data["user"]["email"] == "test@win.com"
      assert response_data["organization"]["name"] == "test123"
    end

    test "and assoc to organization with valid params", %{conn: conn} do
      organization = insert(:organization)

      params = %{
        user: %{
          name: "walter white",
          email: "test@win.com",
          password: "abcpassword"
        },
        organization: %{
          id: organization.id
        }
      }

      conn = post(conn, Routes.account_path(conn, :signup), params)
      response_data = json_response(conn, 201)
      assert response_data["user"]["email"] == "test@win.com"
      assert response_data["organization"]["name"] == organization.name
    end

    test "with a duplicated email", %{conn: conn} do
      {:ok, _} = Accounts.register_user(@valid_attrs)

      params = %{
        user: @valid_attrs,
        organization: %{
          name: "test123"
        }
      }

      conn = post(conn, Routes.account_path(conn, :signup), params)
      response_data_errors = json_response(conn, 422)["errors"]

      assert %{
               "email" => email_errors
             } = response_data_errors

      assert length(email_errors) > 0
    end
  end

  defp create_authenticated_conn(%{conn: conn}) do
    TestHelper.gen_authenticated_admin_conn(conn)
  end

  describe "logout" do
    setup [:create_authenticated_conn]

    test "with valid credentials", %{conn: conn} do
      conn = post(conn, Routes.account_path(conn, :logout))
      assert response(conn, 204)
    end
  end

  describe "socket token" do
    setup [:create_authenticated_conn]

    test "get token", %{conn: conn, session: session} do
      conn = get(conn, Routes.account_path(conn, :get_socket_token))
      response = json_response(conn, 200)["data"]

      assert %{
               "token" => token
             } = response

      session_id = session.id

      assert {:ok, ^session_id} = TokenHelper.verify_websocket_token(conn, token)
    end
  end
end
