defmodule MicroChatWeb.TicketControllerTest do
  use MicroChatWeb.ConnCase
  use Bamboo.Test
  import MicroChat.Factory
  alias MicroChat.TestHelper
  alias MicroChat.Chat.Ticket

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  defp create_authenticated_conn(%{conn: conn}) do
    TestHelper.gen_authenticated_admin_conn(conn)
  end

  defp create_ticket(%{organization: organization}) do
    ticket = insert(:ticket, %{organization: organization})
    {:ok, %{ticket: ticket}}
  end

  defp create_messages(%{ticket: ticket}) do
    messages = insert_list(9, :message, ticket: ticket)
    {:ok, messages: messages}
  end

  describe "index" do
    setup [:create_authenticated_conn, :create_ticket]

    test "lists organization's tickets", %{conn: conn} do
      conn = get(conn, Routes.ticket_path(conn, :index))
      assert [_ticket] = json_response(conn, 200)["data"]
    end

    test "don't lists other organization's tickets", %{conn: conn} do
      insert(:ticket)
      conn = get(conn, Routes.ticket_path(conn, :index))
      assert [_ticket] = json_response(conn, 200)["data"]
    end
  end

  describe "show" do
    setup [:create_authenticated_conn, :create_ticket]

    test "show organization's ticket", %{
      conn: conn,
      ticket: %{id: ticket_id, number: ticket_number}
    } do
      conn = get(conn, Routes.ticket_path(conn, :show, ticket_number))
      assert resp_ticket = json_response(conn, 200)["data"]
      assert %{"id" => ^ticket_id} = resp_ticket
    end

    test "don't show other organization's ticket", %{conn: conn} do
      ticket = insert(:ticket)
      conn = get(conn, Routes.ticket_path(conn, :show, ticket.id))
      assert _resp_ticket = json_response(conn, 404)["errors"]
    end
  end

  describe "create ticket" do
    setup [:create_authenticated_conn]

    test "with agent", %{conn: conn} do
      agent = insert(:user_organization)

      conn =
        post(conn, Routes.ticket_path(conn, :create),
          ticket: %{
            subject: "testing-subject",
            status: 1,
            assigned_agent_id: agent.id,
            contact: %{
              email: "contact@mail.com"
            }
          },
          message: %{
            content: "testing-message"
          }
        )

      assert %{"id" => id, "number" => ticket_number} = json_response(conn, 201)["data"]
      assert_email_delivered_with(subject: ~r/Ticket Assigned -/, to: [agent.user.email])
      assert_email_delivered_with(subject: ~r/Ticket #/, to: ["contact@mail.com"])

      conn = get(conn, Routes.ticket_path(conn, :show, ticket_number))

      assert %{
               "id" => ^id
             } = json_response(conn, 200)["data"]
    end

    test "without agent", %{conn: conn} do
      conn =
        post(conn, Routes.ticket_path(conn, :create),
          ticket: %{
            subject: "testing-subject",
            status: 1,
            assigned_agent_id: nil,
            contact: %{
              email: "contact@mail.com"
            }
          },
          message: %{
            content: "testing-message"
          }
        )

      assert %{"number" => ticket_number} = json_response(conn, 201)["data"]
      assert_email_delivered_with(subject: ~r/Ticket #/, to: ["contact@mail.com"])

      conn = get(conn, Routes.ticket_path(conn, :show, ticket_number))

      assert %{
               "id" => id
             } = json_response(conn, 200)["data"]
    end

    test "try create with invalid data", %{conn: conn} do
      conn = post(conn, Routes.ticket_path(conn, :create), ticket: nil)
      assert response(conn, 400)
    end
  end

  describe "update ticket" do
    setup [:create_authenticated_conn, :create_ticket]

    test "close ticket", %{conn: conn, ticket: %Ticket{id: id} = ticket} do
      conn =
        put(conn, Routes.ticket_path(conn, :update, ticket.number),
          ticket: %{status: Ticket.status_closed()}
        )

      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.ticket_path(conn, :show, ticket.number))
      status_closed = Ticket.status_closed()

      assert %{
               "id" => id,
               "status" => ^status_closed
             } = json_response(conn, 200)["data"]
    end

    test "change agent", %{conn: conn, ticket: %Ticket{id: id} = ticket} do
      %{id: agent_id, user: %{email: agent_email}} = insert(:user_organization)

      conn =
        put(conn, Routes.ticket_path(conn, :update, ticket.number),
          ticket: %{assigned_agent_id: agent_id}
        )

      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.ticket_path(conn, :show, ticket.number))

      assert %{
               "id" => id,
               "assigned_agent_id" => ^agent_id
             } = json_response(conn, 200)["data"]

      assert_email_delivered_with(subject: ~r/Ticket Assigned - #/, to: [agent_email])
    end

    test "renders errors when data is invalid", %{conn: conn, ticket: ticket} do
      conn = put(conn, Routes.ticket_path(conn, :update, ticket.number), ticket: %{status: 9999})
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "get messages list" do
    setup [:create_authenticated_conn, :create_ticket, :create_messages]

    test "lists all messages of ticket", %{conn: conn, ticket: ticket, messages: messages} do
      conn = get(conn, Routes.ticket_path(conn, :get_messages, ticket.number))
      assert length(json_response(conn, 200)["data"]) == length(messages)
    end

    test "Check if they are order by date", %{conn: conn, ticket: ticket, messages: messages} do
      conn = get(conn, Routes.ticket_path(conn, :get_messages, ticket.number))
      messages_response = json_response(conn, 200)["data"]
      assert length(messages_response) == length(messages)

      messages_dates =
        Enum.map(
          messages_response,
          fn message ->
            {:ok, date_sent, 0} = DateTime.from_iso8601(message["inserted_at"])
            date_sent
          end
        )

      assert TestHelper.is_sorted_desc(messages_dates)
    end
  end

  # describe "delete ticket" do
  #   setup [:create_ticket]

  #   test "deletes chosen ticket", %{conn: conn, ticket: ticket} do
  #     conn = delete(conn, Routes.ticket_path(conn, :delete, ticket))
  #     assert response(conn, 204)

  #     assert_error_sent 404, fn ->
  #       get(conn, Routes.ticket_path(conn, :show, ticket))
  #     end
  #   end
  # end
end
