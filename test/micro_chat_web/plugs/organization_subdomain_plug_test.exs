defmodule MicroChatWeb.OrganizationSubdomainPlugTest do
  use MicroChatWeb.ConnCase
  use Plug.Test
  import MicroChat.Factory
  alias MicroChat.TestHelper

  alias MicroChatWeb.OrganizationSubdomainPlug

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "should succeed with right organization id", %{conn: conn} do
    organization = insert(:organization)
    {:ok, conn: tenant_conn} = TestHelper.gen_tenant_organization_conn(conn, "#{organization.id}")

    tenant_conn =
      tenant_conn
      |> put_status(200)
      |> OrganizationSubdomainPlug.call({})

    assert tenant_conn.status == 200
    assert %{assigns: %{current_tenant_organization: assign_organization}} = tenant_conn
    assert assign_organization.id == organization.id
  end

  test "should fail with wrong organization id", %{conn: conn} do
    {:ok, conn: tenant_conn} = TestHelper.gen_tenant_organization_conn(conn, "-1")

    tenant_conn =
      tenant_conn
      |> put_status(200)
      |> OrganizationSubdomainPlug.call({})

    assert tenant_conn.status == 404
  end
end
