defmodule MicroChat.Factory do
  use ExMachina.Ecto, repo: MicroChat.Repo

  def user_factory do
    %MicroChat.Accounts.User{
      email: sequence(:email, &"email-#{&1}@example.com"),
      is_admin_account_created: true
    }
  end

  def role_factory do
    %MicroChat.Accounts.Role{
      name: sequence("serial")
    }
  end

  def organization_factory do
    %MicroChat.Accounts.Organization{
      name: sequence("serial")
    }
  end

  def ticket_organization_seq_tracker_factory do
    %MicroChat.Accounts.TicketOrganizationSeqTracker{
      last_id: 0,
      organization: build(:organization)
    }
  end

  def user_organization_factory do
    %MicroChat.Accounts.UserOrganization{
      organization: build(:organization),
      user: build(:user),
      role_id: sequence(:role_id, [1, 2, 3, 4])
    }
  end

  def session_factory do
    %MicroChat.Accounts.Session{
      user_organization: build(:user_organization)
    }
  end

  def ticket_factory do
    %MicroChat.Chat.Ticket{
      number: sequence(:number, & &1),
      subject: sequence("serial"),
      status: 1,
      organization: build(:organization),
      assigned_agent: build(:user_organization),
      contact: build(:user_organization)
    }
  end

  def message_factory do
    %MicroChat.Chat.Message{
      ticket: build(:ticket),
      user_organization: build(:user_organization)
    }
  end
end
