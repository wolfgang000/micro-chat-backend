defmodule MicroChat.TestHelper do
  import MicroChat.Factory
  import Phoenix.ChannelTest
  @endpoint MicroChatWeb.Endpoint

  def gen_tenant_organization_conn(conn, tenant_subdomain) do
    conn_tenant_subdomain =
      Map.put(
        conn,
        :host,
        tenant_subdomain <>
          "." <> Application.get_env(:micro_chat, MicroChatWeb.Endpoint)[:client_root_host]
      )

    {:ok, conn: conn_tenant_subdomain}
  end

  def gen_authenticated_conn(conn, role_id) do
    organization = insert(:organization)

    insert(:ticket_organization_seq_tracker, %{
      organization: nil,
      organization_id: organization.id
    })

    %{user: user} =
      user_organization =
      insert(:user_organization, %{
        role: nil,
        role_id: role_id,
        organization: nil,
        organization_id: organization.id
      })

    session = insert(:session, %{user_organization: user_organization})

    unauthorize_conn = conn

    conn =
      conn
      |> Plug.Test.init_test_session(%{})
      |> Plug.Conn.put_session(:current_session_id, session.id)

    {:ok,
     conn: conn,
     unauthorize_conn: unauthorize_conn,
     user: user,
     organization: organization,
     user_organization: user_organization,
     session: session}
  end

  def gen_authenticated_admin_conn(conn),
    do: gen_authenticated_conn(conn, MicroChat.Accounts.Role.role_id_admin())

  def gen_authenticated_socket(role_id) do
    user_organization = insert(:user_organization, %{role: nil, role_id: role_id})

    %{
      id: session_id,
      user_organization: %{
        user: user,
        organization: organization
      }
    } = session = insert(:session, user_organization: user_organization)

    socket =
      Phoenix.ChannelTest.socket(MicroChatWeb.UserSocket, "users_socket:#{user.id}", %{
        current_user_id: user.id,
        current_organization_id: organization.id,
        current_user_organization: user_organization,
        current_session_id: session_id
      })

    {:ok,
     socket: socket,
     user: user,
     organization: organization,
     user_organization: user_organization,
     session: session}
  end

  def gen_authenticated_admin_socket,
    do: gen_authenticated_socket(MicroChat.Accounts.Role.role_id_admin())

  def is_sorted_desc([_ | []]), do: true

  def is_sorted_desc([head | tail]) do
    [next_item | _] = tail

    if head >= next_item do
      is_sorted_desc(tail)
    else
      false
    end
  end
end
